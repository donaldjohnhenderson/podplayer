package podplayer.view;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import podplayer.Constants;
import podplayer.controller.ResponseException;

public class PodcastResponse extends AbstractResponse
{
	private final Logger LOG = LoggerFactory.getLogger(PodcastResponse.class);
	
	public PodcastResponse(HttpServletRequest request) 
	{
		super(request);
	}
	
	public String getTemplate()
	{
		return "velocity/podcast_response.vm";
	}

	@Override
	public void process() throws ResponseException 
	{
		String podcastURLIn = getRequestParameter(Constants.HTTPVAR_PODCAST_URL);
		if (podcastURLIn == null)
		{
			LOG.error("No URL supplied at {}", Constants.HTTPVAR_PODCAST_URL);
		}
		else
		{
			LOG.debug("URL supplied as: {}", podcastURLIn);
		}
		
		URL podcastURL = null;
		try
		{
			podcastURL = new URL(podcastURLIn);
		}
		catch (MalformedURLException e)
		{
			LOG.error("URL is not valid", e);
			ResponseException ex = new ResponseException();
			ex.setHttpResponseCode(HttpServletResponse.SC_BAD_REQUEST);
			ex.setUserErrorMessage("This does not look like a URL. Please check the Podcast URL and try again");
			ex.setCause(e);
			ex.setLogErrorMessage(e.getMessage());
			throw ex;
		}

		try
		{
			LOG.debug("Ready to check the feed");
			SyndFeedInput input = new SyndFeedInput();
			XmlReader reader = new XmlReader(podcastURL);
			SyndFeed feed = input.build(reader);
			if (feed == null)
			{
				LOG.error("SyndFeed not returned. Throwing exception");
				ResponseException ex = new ResponseException();
				ex.setHttpResponseCode(HttpServletResponse.SC_NOT_FOUND);
				ex.setUserErrorMessage("The Podcast URL does not appear to be an RSS or ATOM feed. Please check the URL.");
				throw ex;
			}
			else
			{
				if (feed.getEntries().size() < 0)
				{
					LOG.error("SyndFeed returned, but no entries. Throwing exception");
					ResponseException ex = new ResponseException();
					ex.setHttpResponseCode(HttpServletResponse.SC_NOT_FOUND);
					ex.setUserErrorMessage("The Podcast does not have any episodes. Please check the URL.");
					throw ex;
				}
				else
				{
					LOG.debug("SyndFeed returned, with at least one entry. Returning good.");
					addToContext("feed", feed);
				}
			}
		}
		catch (IOException e)
		{
			LOG.error("IOException on read. Unable to read from URL.", e);
			ResponseException ex = new ResponseException();
			ex.setHttpResponseCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			ex.setUserErrorMessage("The Podcast URL does not exist or is not responding. Please check the URL.");
			ex.setCause(e);
			ex.setLogErrorMessage(e.getMessage());
			throw ex;
		}
		catch (FeedException e)
		{
			LOG.error("FeedException on read. Unable to read from URL.", e);
			ResponseException ex = new ResponseException();
			ex.setHttpResponseCode(HttpServletResponse.SC_NOT_FOUND);
			ex.setUserErrorMessage("The Podcast URL supplied did not return valid podcast content. Please check that the URL is an RSS or ATOM feed.");
			ex.setCause(e);
			ex.setLogErrorMessage(e.getMessage());
			throw ex;
		}
	}
	
}
