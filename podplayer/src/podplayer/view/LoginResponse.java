package podplayer.view;

import javax.servlet.http.HttpServletRequest;

import podplayer.Constants;
import podplayer.controller.ResponseException;

public class LoginResponse extends AbstractResponse
{

	public LoginResponse(HttpServletRequest request) 
	{
		super(request);
	}

	@Override
	public String getTemplate() 
	{
		return "velocity/login_prompt_response.vm";
	}

	@Override
	public void process() throws ResponseException 
	{
		String redirectLocation = getRequestParameter(Constants.HTTPVAR_REDIRECT_LOCATION);
		addToContext("redirectLocation", redirectLocation);
		
		String failed = getRequestParameter(Constants.HTTPVAR_LOGIN_FAILED);
		if (failed != null)
		{
			addToContext("loginFailed", true);
		}
	}

}
