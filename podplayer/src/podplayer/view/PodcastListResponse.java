package podplayer.view;

import javax.servlet.http.HttpServletRequest;

import podplayer.controller.ResponseException;
import podplayer.model.User;

public class PodcastListResponse extends AbstractResponse 
{
	private User user;

	public PodcastListResponse(HttpServletRequest request, User user) 
	{
		super(request);
		this.user = user;
	}

	@Override
	public String getTemplate() 
	{
		return "velocity/podcast_list_response.vm";
	}

	@Override
	public void process() throws ResponseException 
	{
		addToContext("podcasts", user.getPodcasts());
	}

}
