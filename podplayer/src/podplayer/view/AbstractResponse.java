package podplayer.view;

import java.io.Writer;

import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import podplayer.controller.ResponseException;

public abstract class AbstractResponse 
{
	
	private final Logger LOG = LoggerFactory.getLogger(AbstractResponse.class);
			
	public static final String DEFAULT_ENCODING = "UTF-8";
	private static VelocityEngine velocity;
	private VelocityContext context;
	private HttpServletRequest request;
	
	public AbstractResponse(HttpServletRequest request) 
	{
		this.request = request;
		context = new VelocityContext();
		if (velocity == null)
		{
			velocity = new VelocityEngine();
			velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
			velocity.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
			velocity.init();
		}
	}
	
	public abstract String getTemplate();
	
	public abstract void process() throws ResponseException;
	
	public void render(Writer writer)
	{
		velocity.mergeTemplate(getTemplate(), DEFAULT_ENCODING, context, writer);
	}
	
	protected void addToContext(String key, Object value)
	{
		context.put(key, value);
	}
	
	protected String getRequestParameter(String key)
	{
		String result = request.getParameter(key);
		if (result == null)
		{
			LOG.error("No value supplied for parameter {}",key);
		}
		return result;
	}
	
	
}
