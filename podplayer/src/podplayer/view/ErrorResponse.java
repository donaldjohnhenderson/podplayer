
package podplayer.view;

import javax.servlet.http.HttpServletRequest;

import podplayer.controller.ResponseException;

public class ErrorResponse extends AbstractResponse
{

	public ErrorResponse(HttpServletRequest request, int code, String message) 
	{
		super(request);
		addToContext("code", code);
		addToContext("message", message);
	}

	@Override
	public String getTemplate() 
	{
		return "velocity/error_response.vm";
	}

	@Override
	public void process() throws ResponseException 
	{
		throw new RuntimeException("No need to call process() on this response");
	}

}
