package podplayer.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import podplayer.Constants;
import podplayer.model.ContentModel;
import podplayer.model.User;

public class Authentication 
{
	private static final Logger LOG = LoggerFactory.getLogger(Authentication.class);

	public static User authenticate(ContentModel contentModel, HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		if (req.getCookies() == null)
		{
			/* If no cookies supplied, no chance of finding a session cookie */
			LOG.debug("No cookies found, redirecting to login");
			Authentication.redirectToLogin(req, resp);
			return null;
		}
		else
		{
			for (Cookie cookie : req.getCookies())
			{
				if (cookie.getName().equals(Constants.COOKIE_NAME_PODPLAYER_SESSION))
				{
					/* If a session cookie is suplied, check it is valid */
					LOG.debug("Session cookie supplied, going to check validity");
					User theUser = contentModel.validateSession(cookie.getValue(), req.getRemoteAddr());
					if (theUser == null)
					{
						LOG.debug("Unable to validate session id, redirecting to login");
						redirectToLogin(req, resp);
					}
					else
					{
						LOG.debug("Session id is valid");
						return theUser;
					}
				}
			}
		}

		/* No valid session cookie found, cannot be logged in */ 
		return null;
		
	}
	
	private static void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws Exception
	{
		LOG.info("Setting login redirect location to the current path");
		String redirectLocation = req.getServletPath()+"?"+req.getQueryString();
		LOG.debug("Redirecting to: {}", redirectLocation);
		resp.sendRedirect("/podplayer/login?"+Constants.HTTPVAR_REDIRECT_LOCATION+"="+redirectLocation);
	}
	
}
