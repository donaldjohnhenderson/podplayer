package podplayer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import podplayer.Constants;
import podplayer.model.ContentModel;
import podplayer.model.User;
import podplayer.view.AbstractResponse;
import podplayer.view.ErrorResponse;
import podplayer.view.PodcastListResponse;
import podplayer.view.PodcastResponse;

public class PodcastServlet extends HttpServlet 
{
	private final Logger LOG = LoggerFactory.getLogger(PodcastServlet.class);
	private static final long serialVersionUID = -415070568566948502L;
	
	private ContentModel contentModel;
	
	@Override
	public void init() throws ServletException 
	{
		super.init();
		contentModel = new ContentModel();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		LOG.info("Request from IP address [{}] ",req.getRemoteAddr());
		
		User theUser = null;
		
		try
		{
			theUser = Authentication.authenticate(contentModel, req, resp);
			if (theUser == null)
			{
				/* The authenticator has already supplied a redirect location */
				return;
			}
		}
		catch (Exception e)
		{
			LOG.error("Unable to authenticate user. Baling out and returning 500");
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		
		String function = req.getParameter(Constants.HTTPVAR_FUNCTION);
		if (function == null)
		{
			LOG.error("No function supplied - cannot continue");
			ErrorResponse errorResponse = new ErrorResponse(req, HttpServletResponse.SC_BAD_REQUEST, "No function supplied, cannot continue");
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			errorResponse.render(resp.getWriter());
			return;
		}
		
		AbstractResponse responseModel = null;
		
		switch (function)
		{
			case "listPodcasts":
				responseModel = new PodcastListResponse(req, theUser);
				break;
				
			case "displayPodcast": 
				responseModel = new PodcastResponse(req);
				break;
		}
		
		try
		{
			LOG.debug("Processing ResponseModel");
			responseModel.process();
			responseModel.render(resp.getWriter());
		}
		catch (ResponseException e)
		{
			LOG.error("Unable to process the Response", e);
			ErrorResponse errorResponse = new ErrorResponse(req, e.getHttpResponseCode(), e.getUserErrorMessage());
			resp.setStatus(e.getHttpResponseCode());
			errorResponse.render(resp.getWriter());
		}
	}
	
}
