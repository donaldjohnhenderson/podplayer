package podplayer.controller;

public class ResponseException extends Exception
{

	private static final long serialVersionUID = -3537427705189555419L;

	private int httpResponseCode;
	private String userErrorMessage;
	private String logErrorMessage;
	private Exception cause;
	
	/*
	 * Auto-generated getters and setters
	 */
	public int getHttpResponseCode() {
		return httpResponseCode;
	}
	public void setHttpResponseCode(int httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}
	public String getUserErrorMessage() {
		return userErrorMessage;
	}
	public void setUserErrorMessage(String userErrorMessage) {
		this.userErrorMessage = userErrorMessage;
	}
	public String getLogErrorMessage() {
		return logErrorMessage;
	}
	public void setLogErrorMessage(String logErrorMessage) {
		this.logErrorMessage = logErrorMessage;
	}
	public Exception getCause() {
		return cause;
	}
	public void setCause(Exception cause) {
		this.cause = cause;
	}
	
}
