package podplayer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import podplayer.model.User;
import podplayer.Constants;
import podplayer.model.ContentModel;
import podplayer.view.LoginResponse;

public class LoginServlet extends HttpServlet
{
	private final Logger LOG = LoggerFactory.getLogger(LoginServlet.class);
	private static final long serialVersionUID = -8211396985589854358L;
	
	private ContentModel contentModel;
	
	@Override
	public void init() throws ServletException 
	{
		contentModel = new ContentModel();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		
		String logout = req.getParameter(Constants.HTTPVAR_LOGOUT);
		if (logout != null)
		{
			/* Send a blank cookie to delete the session cookie on the client 
			 * In a real implementation we would purge the session ID from storage as well */
			Cookie logoutCookie = new Cookie(Constants.COOKIE_NAME_PODPLAYER_SESSION, "");
			logoutCookie.setMaxAge(0);
			resp.addCookie(logoutCookie);
			resp.sendRedirect("/");
			return;
		}
		
		try
		{
			LoginResponse loginResponse = new LoginResponse(req);
			loginResponse.process();
			loginResponse.render(resp.getWriter());
		}
		catch (Exception e)
		{
			LOG.error("Unable to server LoginResponse");
			resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		
		String redirectLocation = null;
		redirectLocation = req.getParameter(Constants.HTTPVAR_REDIRECT_LOCATION);
		
		String username = req.getParameter(Constants.HTTPVAR_USERNAME);
		String password = req.getParameter(Constants.HTTPVAR_PASSWORD);
		
		LOG.debug("Username is {}", username);
		LOG.debug("Was a password supplied? ", (password != null));
		
		if (username == null || password == null)
		{
			LOG.debug("Username or password not supplied");
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		User validCredentials = contentModel.validateCredentials(username, password);
		if (validCredentials == null)
		{
			LOG.debug("Unable to authenticate user {}", username);
			resp.sendRedirect("/podplayer/login?failed=true");
		}
		else
		{
			LOG.debug("Valid credentials, going to generate and return a session id");
			String newSessionId = contentModel.generateAndSaveSessionIdForUserAndIp(username, req.getRemoteAddr());
			resp.addCookie(new Cookie(Constants.COOKIE_NAME_PODPLAYER_SESSION, newSessionId));
			LOG.debug("Assigning session ID {} to user {}", newSessionId, username);
		}
		
		try
		{
			doRedirect(resp, redirectLocation);
		}
		catch (Exception e)
		{
			LOG.error("Unable to do redirect");
		}
	}
	
	private void doRedirect(HttpServletResponse resp, String redirectLocation) throws Exception
	{
		if (redirectLocation == null)
		{
			resp.sendRedirect("/");
			return;
		}
		else
		{
			LOG.debug("Redirecting to specified location: {}", redirectLocation);
			resp.sendRedirect(redirectLocation);
			return;
		}	
	}
	
}
