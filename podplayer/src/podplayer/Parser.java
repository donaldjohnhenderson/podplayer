package podplayer;

import java.net.URL;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

public class Parser 
{
	
	public static void main(String[] args) throws Exception
	{
		String url = "http://www.bbc.co.uk/programmes/p02nrvj8/episodes/downloads.rss";
		SyndFeedInput input = new SyndFeedInput();
		SyndFeed feed = input.build(new XmlReader(new URL(url)));
		
		System.out.println("Name: "+feed.getTitle());
		System.out.println("Description: "+feed.getDescription());
		System.out.println("Author: "+feed.getAuthor());
		for (SyndEntry entry : feed.getEntries())
		{
			System.out.println("EPISODE: ");
			System.out.println("Title: "+entry.getTitle());
			System.out.println("URL: "+entry.getLink());
			System.out.println("URL2: "+entry.getEnclosures().get(0).getUrl());
			
		}
	}
}
