package podplayer.model;

import java.util.ArrayList;
import java.util.List;

public class ContentModel 
{
	
	private List<User> users;
	
	public ContentModel() 
	{
		users = new ArrayList<User>();
		users.add(new User().username("donald").ipAddress("127.0.01").sessionId("7826").password("music")
				.podcast(new Podcast().name("The Chris Evans Breakfast Show").url("http://www.bbc.co.uk/programmes/p02nrvj8/episodes/downloads.rss"))
				.podcast(new Podcast().name("Atlantic Voice").url("http://www.cbc.ca/podcasting/includes/maritimemagazine.xml"))
				.podcast(new Podcast().name("Comedy Factory").url("http://www.cbc.ca/podcasting/includes/cf.xml"))
				.podcast(new Podcast().name("FT Tech Tonic").url("http://rss.acast.com/ft-tech-tonic"))
				.podcast(new Podcast().name("Guardian Long Reads").url("https://www.theguardian.com/news/series/the-audio-long-read/podcast.xml"))
				);
		users.add(new User().username("chris").ipAddress("127.0.01").sessionId("3941").password("letmein")
				.podcast(new Podcast().name("Nick Grimshaw").url("http://www.bbc.co.uk/programmes/p02nrvjf/episodes/downloads.rss"))
				.podcast(new Podcast().name("Hidden Brain").url("http://www.npr.org/rss/podcast.php?id=510308"))
				.podcast(new Podcast().name("FT Politics").url("http://podcast.ft.com/s/ft-politics/feed/"))
				.podcast(new Podcast().name("Maritime Noon").url("http://www.cbc.ca/podcasting/includes/maritimenoon.xml"))
				.podcast(new Podcast().name("Science Weekly").url("https://www.theguardian.com/science/series/science/podcast.xml"))
				);
	}
	
	public User validateSession(String sessionId, String ipAddress)
	{
		/*
		 * This example implementation just looks in our list of pre-defined users
		 * In a real implementation, we would cache the session IDs in a KV store, database, or similar
		 */
		for (User user : users)
		{
			if (user.getSessionId().equals(sessionId))
			{
				return user;
			}	
		}
		return null;
	}
	
	public User validateCredentials(String username, String password)
	{
		/*
		 * This example implementation has two pre-defined users. 
		 * It shows how the password would be salted, hashed and stored. 
		 * A real implementation would behave in a similar way, but use a database for lookups
		 */
		for (User user : users)
		{
			if (user.getUsername().equals(username))
			{
				String hashWithSuppliedPassword = user.getPasswordHash(password, user.getPasswordSalt());
				String hashWithActualPassword = user.getPasswordHash();
				if (hashWithActualPassword.equals(hashWithSuppliedPassword))
				{
					return user;
				}
			}
		}
		return null;
	}
	
	public String generateAndSaveSessionIdForUserAndIp(String username, String ipAddress)
	{
		/*
		 * This example implementation just returns the pre-defined session ID
		 * In a real implementation, we would generate a random session ID and 
		 *  persist it in the database. 
		 */
		for (User user : users)
		{
			if (user.getUsername().equals(username))
			{
				return user.getSessionId();
			}
		}
		return null;
	}

}
