package podplayer.model;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User 
{
	private final Logger LOG = LoggerFactory.getLogger(User.class);
			
	private String username;
	private String sessionId;
	private String ipAddress;
	private String passwordHash;
	private String passwordSalt;
	
	private List<Podcast> podcasts = new ArrayList<Podcast>();
	
	private static final SecureRandom secureRandom = new SecureRandom();
	
	public void addPodcastToUser(Podcast podcast)
	{
		podcasts.add(podcast);
	}
	
	public String getPasswordHash(String password, String salt)
	{
		String saltAndPassword = salt + password;
		try
		{
			MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
			sha256.update(saltAndPassword.getBytes("UTF-8")); 
			return new String(sha256.digest());
		}
		catch (Exception e)
		{
			LOG.error("Unable to hash the password", e);
			return null;
		}
		
	}
	
	/*
	 * Builder pattern getters and setters
	 */
	public User username(String username)
	{
		this.username = username; 
		return this;
	}
	
	public User sessionId(String sessionId)
	{
		this.sessionId = sessionId; 
		return this;
	}
	
	public User ipAddress(String ipAddress)
	{
		this.ipAddress = ipAddress; 
		return this;
	}
	
	public User password(String password)
	{
		byte[] saltBytes = new byte[16];
		secureRandom.nextBytes(saltBytes);
		passwordSalt = new String(saltBytes);
		passwordHash = getPasswordHash(password, passwordSalt);
		return this;
	}
	
	public User podcast(Podcast podcast)
	{
		addPodcastToUser(podcast);
		return this;
	}
	
	/*
	 * Auto-generated getters and setters
	 */
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public List<Podcast> getPodcasts() {
		return podcasts;
	}

}
