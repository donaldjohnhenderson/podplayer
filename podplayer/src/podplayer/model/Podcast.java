package podplayer.model;

public class Podcast 
{
	private String name;
	private String url;
	
	/*
	 * Builder pattern getters and setters
	 */
	public Podcast name(String name)
	{
		setName(name);
		return this;
	}
	
	public Podcast url(String url)
	{
		setUrl(url);
		return this;
	}
	
	
	/*
	 * Auto-generated getters and setters
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
