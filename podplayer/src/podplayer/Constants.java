package podplayer;

public class Constants 
{

	public static final String COOKIE_NAME_PODPLAYER_SESSION = "podplayer-session-id";
	public static final String HTTPVAR_REDIRECT_LOCATION = "redirect_location";
	public static final String HTTPVAR_USERNAME = "username";
	public static final String HTTPVAR_PASSWORD = "password";
	public static final String HTTPVAR_LOGIN_FAILED = "failed";
	public static final String HTTPVAR_LOGOUT = "logout";
	public static final String HTTPVAR_FUNCTION = "function";
	public static final String HTTPVAR_PODCAST_URL = "podcast_url";

}
